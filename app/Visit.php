<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $guarded = [];

    public static function mark() {
        $visit = new Visit([
            'server' => request()->getHost(),
        ]);
        $visit->save();
    }
}
